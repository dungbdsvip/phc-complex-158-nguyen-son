# PHC Complex 158 Nguyễn Sơn
Dự án [PHC Complex](http://northernhomes.vn/shophouse-lien-ke-chung-cu-cao-cap-phc-complex-158-nguyen-son.html) tọa lạc tại vị trí đắc địa trung tâm giao thương khu vực quận Long Biên, Hà Nội, khu phố hàng không 158 Nguyễn Sơn với hệ thống cơ sở hạ tầng phát triển hiện đại đồng bộ điện, đường, trường trạm. Dự án tổ hợp bởi 48 lô liền kề shophouse thương mại, 117 căn hộ chung cư cao cấp, 4 tầng trung tâm thương mại tích hợp 2 tầng hầm rộng rãi đáp ứng mọi nhu cầu để ở hay để đầu tư của quý khách hàng.

🔸 Tên dự án: Trung tâm thương mại, văn phòng cho thuê, biệt thự liền kề và căn hộ cao cấp  [PHC Complex 158 Nguyễn Sơn](http://northernhomes.vn/shophouse-lien-ke-chung-cu-cao-cap-phc-complex-158-nguyen-son.html) .
🔸  Tên thương mại: PHC Complex .
🔸  Vị trí dự án: Số 158 đường Nguyễn Sơn, phương Bồ Đề, quận Long Biên, TP. Hà Nội.
🔸  Chủ đầu tư: Công ty TNHH Bắc Chương Dương.
🔸  Đơn vị giám sát thi công: Công ty cổ phần đầu tư và xây lắp Hà Nội.
🔸  Nhà thầu thi công: Công ty cổ phần Confitec 3 và Công ty cổ phần xây dựng số 5 Hà Nội.
🔸  Tổng diện tích : 8000m2
🔸  Diện tích trồng cây xanh : 2.500m2
🔸  Diện tích đất Shophouse liền kề: 4.000m2
🔸  Diện tích đất xây chung cư: 2.572m2
Nguồn bài viết tại : [http://northernhomes.vn/shophouse-lien-ke-chung-cu-cao-cap-phc-complex-158-nguyen-son.html](http://northernhomes.vn/shophouse-lien-ke-chung-cu-cao-cap-phc-complex-158-nguyen-son.html)